####################################
# XV. Thesaurus Entries            #
####################################
import json

thesaurus = {}

def t(baseword):
    return thesaurus[baseword]

with open('data/thesaurus.json', 'r') as x:
    f = x.read()
    thesaurus = json.loads(f)